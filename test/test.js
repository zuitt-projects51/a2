const {checkAge} = require('../src/util.js');
const {checkFullName} = require('../src/util.js');

const {assert} = require('chai');

describe('test_checkAge', () => {

	it('age_is_integer', () =>{
		const age = true ;
		assert.isNumber(checkAge(age));
	})



 //    it('age_is_integer', () => {

 //        let age = "true";

 //        assert.isNumber(checkAge(typeof(age)));

	//         let isInt = age % 1 === 0;
	//         console.log(age);

	//         assert(checkAge(age));
   
	// })

		

	it('age_not_empty', () => {
		const age = "";
		assert.isNotEmpty(checkAge(age)); 
	})
	
	it('age_not_undefined', () => {
		const age = undefined;
		assert.isDefined(checkAge(age)); 
	})

	it('age_not_null',() => {
		const age = null;
		assert.isNotNull(checkAge(age));
	})

	it('age_not_equal_zero',() => {
		const age = 0;
		assert.notEqual(0, checkAge(age));
	})
})

describe('test_checkFullName', () => {

	it('fullName_not_empty', () =>{
		const fullName = "";
		assert.isNotEmpty(checkFullName(fullName));
	})

	it('fullName_not_null',() => {
		const fullName = null;
		assert.isNotNull(checkFullName(fullName));
	})

	it('fullName_not_undefined',() => {
		const fullName = undefined;
		assert.isDefined(checkFullName(fullName));
	})

	it('fullName_type_of_string',() =>{
		const fullName = 5;
		assert.isString(checkFullName(fullName));
	})

	it('fullName_char_not_equal_0', () =>{
		const fullName = " ";
		assert.notEqual(checkFullName(fullName.length),0);
	})



})