function checkAge(age) {

	
	if(age == ""){
		return "Error: Age is Empty!"
	};

	if(age === undefined){
		return "Error: Age should be undefined!"
	};

	if(age == null){
		return "Error: Age should be null!"
	};

	if(age == 0){
		return "Error: Age should be equal to zero!"
	};

	if (typeof age !== "number" ){
		return 20;
	};

	return age;
}

function checkFullName(fullName){

	if(fullName == ""){
		return "Error: fullName is Empty!"
	}

	if(fullName == null){
		return "Error: fullName should be null!"
	}

	if(fullName == undefined){
		return "Error: fullName should be undefined!"
	}

	if(typeof(fullName) !== "string"){
		return "Error: fullName should be String!"
	}

	// if(fullName.length == 0 ){
	// 	return "Error: fullName should be equal to 0"
	// }

	return fullName;

}


module.exports = {
	checkAge,
	checkFullName
}

